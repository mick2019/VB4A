VERSION 5.00
Begin VB.Form Form5 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "大小写检查"
   ClientHeight    =   495
   ClientLeft      =   7320
   ClientTop       =   7260
   ClientWidth     =   4680
   LinkTopic       =   "Form5"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   495
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  '窗口缺省
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "0"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   12
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   4575
   End
End
Attribute VB_Name = "Form5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Checkstrline(1 To 10000) As String
Dim Checkword(1 To 3000) As String
Public Linecounter As Integer
Public wordcounter As Integer


    

Public Sub OnlineCheck(Checkname As String)
    
    Linecounter = 0
    wordcounter = 0
    
    For i = 1 To 10000
        Checkstrline(i) = ""
    Next i
    
    Linecounter = 0
    Open Checkname For Input As #1
    
    Do While Not EOF(1)
        Linecounter = Linecounter + 1
        Line Input #1, Checkstrline(Linecounter)
    Loop
    
    Close #1
    
    'Beep
    
    Open Checkname For Output As #1
    
    For i = 1 To Linecounter
        
        Print #1, Change(Checkstrline(i))
        Me.Label1.Caption = Int(i * 100 / Linecounter) & "%"
        
    Next i
    
    Close #1
    'Beep
    'Unload Me
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "OnlineCheck", vbExclamation
End Sub


    

Public Sub execheck(Checkname As String)
    
    Linecounter = 0
    wordcounter = 0
    
    For i = 1 To 10000
        Checkstrline(i) = ""
    Next i
    
    Linecounter = 0
    Open App.Path + "\" + Form1.projectName + "\src\com\" + LCase(Form1.projectName) + "\" + Checkname + ".simple" For Input As #1
    
    Do While Not EOF(1)
        Linecounter = Linecounter + 1
        Line Input #1, Checkstrline(Linecounter)
    Loop
    
    Close #1
    
    'Beep
    
    Open App.Path + "\" + Form1.projectName + "\src\com\" + LCase(Form1.projectName) + "\" + Checkname + ".simple" For Output As #1
    
    For i = 1 To Linecounter
        
        Print #1, Change(Checkstrline(i))
        Me.Label1.Caption = Int(i * 100 / Linecounter) & "%"
        
    Next i
    
    Close #1
    'Beep
    Unload Me
    
    If Form1.Silently = False Then
        If Form1.CH Then
            If MsgBox("是否开始编译？", vbInformation + vbYesNo, "") = vbYes Then Call Form1.compile_Click
        Else
            If MsgBox("Compile?", vbInformation + vbYesNo, "") = vbYes Then Call Form1.compile_Click
        End If
    End If
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "execheck", vbExclamation
End Sub


    

Public Function Change(Str As String) As String
    'For i = 1 To 10000
    'Checkstrline(i) = ""
    'Next i
    Dim KeyWordsMapCH As String
    Dim KeyWordsMapEN As String
    
    KeyWordsMapCH = "别名|且|为|布尔型|按索引|字节型|按值|条件|定义常量|日期|定义|双精度型|每个|否则|要是|结束|错误|事件|退出|计次循环|函数|Get|如果|中|整形|是|不是|相似|长整形|Me|静态|步进|字符型|过程|那么|到|获得类型|直到|可变型|当|抑或"
    KeyWordsMapEN = "Alias|And|As|Boolean|ByRef|Byte|ByVal|Case|Const|Date|Dim|Double|Each|Else|ElseIf|End|Error|Event|Exit|For|Function|Get|If|In|Integer|Is|IsNot|Like|Long|Me|Static|Step|String|Sub|Then|To|TypeOf|Until|Variant|While|Xor"
    
    For i = 1 To 3000
        Checkword(i) = ""
    Next i
    Change = ""
    Dim strtmp As String
    strtmp = ""
    
    wordcounter = 1
    For i = 1 To Len(Str)
        strtmp = Mid(Str, i, 1)
        Select Case strtmp
            
        Case " ", "=", "+", "-", "*", "/", "(", ")", ":", ">", "<", "&", "^", ".", vbTab
            'Case " "
            wordcounter = wordcounter + 1
            Checkword(wordcounter) = strtmp
            wordcounter = wordcounter + 1
        Case Else
            Checkword(wordcounter) = Checkword(wordcounter) & strtmp
            
        End Select
        
    Next i '划分单词
    
    
    For i = 1 To wordcounter
        
        Select Case LCase(Checkword(i))
            
        Case "split", "boolean", "byte", "short", "integer", "long", "single", "double", "string", "date", "object", "variant", "true", "false", "format"
            
            Checkword(i) = UCase(Mid(Checkword(i), 1, 1)) & LCase(Mid(Checkword(i), 2, Len(Checkword(i)) - 1))
            
        Case "alias", "and", "not", "next", "as", "case", "const", "dim", "each", "else", "end", "error", "event", "exit", "for", "function", "get", "if", "in", "is", "like", "me", "static", "step", "sub", "then", "to", "until", "while", "xor", "new", "on"
            Checkword(i) = UCase(Mid(Checkword(i), 1, 1)) & LCase(Mid(Checkword(i), 2, Len(Checkword(i)) - 1))
            
        Case "available", "enabled", "text", "value", "hint", "files", "application", "math", "acc", "quit", "hide"
            Checkword(i) = UCase(Mid(Checkword(i), 1, 1)) & LCase(Mid(Checkword(i), 2, Len(Checkword(i)) - 1))
            
        Case "ceil2", "floor2", "round2", "ceil", "asin", "acos", "floor", "round", "int", "abs", "cos", "exp", "log", "max", "min", "rnd", "sin", "sgn", "sqr", "tan"  '函数
            Checkword(i) = UCase(Mid(Checkword(i), 1, 1)) & LCase(Mid(Checkword(i), 2, Len(Checkword(i)) - 1))
            
        Case "button", "image", "canvas", "panel", "label", "unpack"
            Checkword(i) = UCase(Mid(Checkword(i), 1, 1)) & LCase(Mid(Checkword(i), 2, Len(Checkword(i)) - 1))
            
        Case "textbox"
            Checkword(i) = "TextBox"
            
        Case "ascw"
            Checkword(i) = "AscW"
            
        Case "chrw"
            Checkword(i) = "ChrW"
            
        Case "passwordtextbox"
            Checkword(i) = "PasswordTextBox"
            
        Case "dates", "strings"
            Checkword(i) = UCase(Mid(Checkword(i), 1, 1)) & LCase(Mid(Checkword(i), 2, Len(Checkword(i)) - 1))
            
        Case "day", "hour", "minute", "month", "now", "second", "timer", "weekday", "year" 'dates函数集1
            Checkword(i) = UCase(Mid(Checkword(i), 1, 1)) & LCase(Mid(Checkword(i), 2, Len(Checkword(i)) - 1))
            
        Case "left", "len", "mid", "replace", "right", "trim", "msgbox", "conversions", "asc", "hex", "chr", "finish", "str", "val" 'strings函数集1
            Checkword(i) = UCase(Mid(Checkword(i), 1, 1)) & LCase(Mid(Checkword(i), 2, Len(Checkword(i)) - 1))
            
        Case "lcase", "ltrim", "rtrim", "ucase", "vprop" 'strings函数集2
            Checkword(i) = UCase(Mid(Checkword(i), 1, 2)) & LCase(Mid(Checkword(i), 3, Len(Checkword(i)) - 1))
            
        Case "texts", "number", "datetime", "null", "phonenumber"
            Checkword(i) = UCase(Checkword(i))
        Case "lsensor"
            Checkword(i) = "LSensor"
        Case "tsensor"
            Checkword(i) = "TSensor"
        Case "vb4ainput"
            Checkword(i) = "VB4AInput"
            
        Case "dateadd"
            Checkword(i) = "DateAdd"
            
        Case "datevalue"
            Checkword(i) = "DateValue"
            
        Case "formatdate"
            Checkword(i) = "FormatDate"
            
        Case "monthname"
            Checkword(i) = "MonthName"
            
        Case "weekdayname"
            Checkword(i) = "WeekdayName"
            
        Case "byref"
            Checkword(i) = "ByRef"
            
        Case "byval"
            Checkword(i) = "ByVal"
            
        Case "elseif"
            Checkword(i) = "ElseIf"
            
        Case "isnot"
            Checkword(i) = "IsNot"
            
        Case "typeof"
            Checkword(i) = "TypeOf"
            
        Case "myphone"
            Checkword(i) = "MyPhone"
            
        Case "mytimer"
            Checkword(i) = "MyTimer"
            
        Case "gps"
            Checkword(i) = "GPS"
            
        Case "orient"
            Checkword(i) = "ORIENT"
            
        Case "toastmessage"
            Checkword(i) = "ToastMessage"
            
        Case "gettime"
            Checkword(i) = "GetTime"
            
        Case "getdate"
            Checkword(i) = "GetDate"
            
        Case "sendsms"
            Checkword(i) = "SendSMS"
            
        Case "jumpurl"
            Checkword(i) = "JumpURL"
            
        Case "sendmail"
            Checkword(i) = "SendMail"
            
        Case "degreestoradians"
            Checkword(i) = "DegreesToRadians"
            
        Case "radianstodegrees"
            Checkword(i) = "RadiansToDegrees"
            
        Case "instr"
            Checkword(i) = "InStr"
            
        Case "instrrev"
            Checkword(i) = "InStrRev"
            
        Case "strcomp"
            Checkword(i) = "StrComp"
            
        Case "strreverse"
            Checkword(i) = "StrReverse"
            
        Case "endif"
            Checkword(i) = "End If"
            
        Case "vb4ainputboxshow"
            Checkword(i) = "VB4AInputBoxShow"
            
        Case "vb4ainputboxclicked"
            Checkword(i) = "VB4AInputBoxClicked"
            
        Case "vb4amsgboxshow"
            Checkword(i) = "VB4AMsgboxShow"
        
        Case "vb4amsgboxclicked"
            Checkword(i) = "VB4AMsgboxClicked"
            
        End Select
        
        '中文编程实现
        
        If Form1.HasCh(Checkword(i)) = True Then
            
            For j = 0 To UBound(Split(KeyWordsMapCH, "|"))
                
                If Checkword(i) = Split(KeyWordsMapCH, "|")(j) Then Checkword(i) = Split(KeyWordsMapEN, "|")(j)
                
            Next j
            
        End If
        
    Next i '大小写转换
    
    For i = 1 To wordcounter
        
        Change = Change & Checkword(i)
    Next i
    
    
    

    Exit Function
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Function: " & vbTab & "Change", vbExclamation
End Function

Private Sub Form_Load()
    On Error GoTo ErrHand

    Call Form1.lan_changE(Form1.CH)

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub

