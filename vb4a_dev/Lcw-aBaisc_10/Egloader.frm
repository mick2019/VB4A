VERSION 5.00
Begin VB.Form Egloader 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "示例程序"
   ClientHeight    =   3090
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4680
   LinkTopic       =   "Form8"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  '屏幕中心
   Begin VB.CommandButton Command1 
      Caption         =   "打开"
      Height          =   2415
      Left            =   3960
      TabIndex        =   2
      Top             =   480
      Width           =   615
   End
   Begin VB.TextBox Text1 
      Height          =   2415
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   480
      Width           =   3735
   End
   Begin VB.ComboBox Combo1 
      Height          =   300
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4455
   End
End
Attribute VB_Name = "Egloader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Combo1_Click()
    If Dir(App.Path & "\examples\" & Me.Combo1.text & ".txt") <> "" Then
    On Error GoTo ErrHand

        Open App.Path & "\examples\" & Me.Combo1.text & ".txt" For Input As #21
        Line Input #21, Desc
        Close #21
        
        If Form1.CH = True Then
            Text1.text = Split(Desc, "|")(0)
        Else
            Text1.text = Split(Desc, "|")(1)
        End If
    End If

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Combo1_Click", vbExclamation
End Sub


    

Private Sub Command1_Click()
    
    If Me.Combo1.text <> "" Then
        Form1.LoadProject (App.Path & "\examples\" & Me.Combo1.text)
    Else
        
        MsgBox Form1.CHorEN(Form1.CH, "未选择示例程序！|No Example Choosed!"), vbCritical + vbOKOnly, ""
        
        
    End If
    
    Me.Hide
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command1_Click", vbExclamation
End Sub

Private Sub Form_Load()
    On Error GoTo ErrHand

    Dim MyFile As String, strFile As String
    
    strFile = App.Path & "\examples\*.v4a"
    MyFile = Dir(strFile, vbNormal)
    Me.Combo1.Clear
    Do While MyFile <> ""
        If InStr(1, MyFile, StrZf) > 0 Then Combo1.AddItem MyFile
        MyFile = Dir
    Loop
    
    If Form1.CH = True Then
        Me.Caption = "示例程序"
        Me.Command1.Caption = "打开"
    Else
        Me.Caption = "Examples"
        Me.Command1.Caption = "Open"
    End If
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub
