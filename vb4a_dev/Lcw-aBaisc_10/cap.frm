VERSION 5.00
Begin VB.Form cap 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form8"
   ClientHeight    =   1590
   ClientLeft      =   9405
   ClientTop       =   3945
   ClientWidth     =   3030
   LinkTopic       =   "Form8"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1590
   ScaleWidth      =   3030
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   1455
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   2775
      Begin VB.CommandButton Command1 
         Caption         =   "Command1"
         Height          =   855
         Left            =   1320
         TabIndex        =   3
         Top             =   360
         Width           =   1215
      End
      Begin VB.OptionButton Option2 
         Caption         =   "OFF"
         Height          =   495
         Left            =   240
         TabIndex        =   2
         Top             =   840
         Width           =   1815
      End
      Begin VB.OptionButton Option1 
         Caption         =   "ON"
         Height          =   495
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   1575
      End
   End
End
Attribute VB_Name = "cap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
    If Me.Option1.value = True Then
    On Error GoTo ErrHand

        SaveSetting "VB4A", "Capi", "CM", True
    Else
        SaveSetting "VB4A", "Capi", "CM", False
    End If
    Me.Hide
    If Form1.CH Then
        If MsgBox("兼容模式的修改需要重启程序方能生效，是否立即关闭程序？", vbYesNo + vbQuestion, "") = vbYes Then End
    Else
        If MsgBox("Changing in Compatibility mode needs program restart, Exit now?", vbYesNo + vbQuestion, "") = vbYes Then End
    End If

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command1_Click", vbExclamation
End Sub

Private Sub Form_Load()
    On Error GoTo ErrHand

    If Form1.CH Then Me.Caption = "兼容性设置" Else Me.Caption = "Compatibility mode setup"
    If Form1.CH Then Me.Command1.Caption = "确定" Else Me.Command1.Caption = "Done"
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub
